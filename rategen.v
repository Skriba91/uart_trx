`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    19:38:00 02/11/2017 
// Design Name: 
// Module Name:    rategen 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rategen
	#(parameter SIZE = 8, RATE_DIV = 1)
	(
    input clk,
    input rst,
    output rate
    );
	 
	 reg [SIZE:0] Q;
	 
	 always @(posedge clk)
	 begin
		if(rst | rate)
			Q <= 0;
		else
			Q <= Q + 1;
	 end
	 
	 assign rate = (Q == (RATE_DIV-1));


endmodule
