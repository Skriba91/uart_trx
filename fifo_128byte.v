`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Skriba D�niel
// 
// Create Date:    18:21:53 02/25/2017 
// Design Name: 
// Module Name:    fifo_128byte 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 128byte fifo
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module fifo_128byte(
    input clk,						//clock
    input rst,						//reset signal
	 input write,					//data is ready to write
    input read,					//data was read
    input [7:0] data_in,		//input data line
    output data_av,				//output data available
	 output full,					//fifo is full
    output [7:0] data_out		//output data line
    );
	 
	 
	 reg [6:0] read_addr;			//The starting address of read
	 reg [6:0] write_addr;			//The starting address of write
	 reg [7:0] data [0:127];		//Memory TODO: set to 127
	 
	 //Block for memory managemant
	 always @(posedge clk)
	 begin
		if(rst)	//reset resets the fifo
			read_addr <= 0;
		else if(read && (read_addr == 127))
			read_addr <= 0;
		else if(read)
			read_addr <= read_addr + 1;
	 end
	 
	 always @(posedge clk)
	 begin
		if(write)
			data[write_addr] <= data_in;
	 end
	 
	 always @(posedge clk)
	 begin
		if(rst)
			write_addr <= 0;
		else if(write && (write_addr == 127))
			write_addr <= 0;
		else if(write)
			write_addr <= write_addr + 1;
	 end
		
				
	 assign full = ((write_addr - read_addr) == 127);	//If the differenc beatween write and read address is equals the size of the fifo then the fifo is full
	 assign data_av = (read_addr != write_addr);			//If wire_addr is equals to read_addr it means all data was read out
	 assign data_out = data[read_addr];					//The data on the output line is the data on the first address
			


endmodule
