`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    12:15:36 02/27/2017 
// Design Name: 
// Module Name:    rx_data_cntr 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module rx_data_cntr
	#(parameter PARITY = 1'b0,
	  parameter PARITY_BIT = 1'b0,
	  parameter IDLE = 2'b0,			//Begining state. Input line is high.
	  parameter STARTING = 2'b01,	//When input line goes from high to low in IDLE state
	  parameter RECIEVING = 2'b10,	//When zero was read in starting state
	  parameter FINISHED = 2'b11	//Stop bit recieved.
	 )
	(
    input clk,					//clock
    input rst,					//reset
    input rate,				//sampling rate
    input in,					//input line
    output write,				//Input buffer write
    output err,				//Error flag
    output [7:0] data_out	//Recieved data
    );
	 
	 reg [15:0] sample;			//Stores the line samples
	 reg [9:0] data;				//Storec the recieved data. After a read cycle the LSB will be the start bit and the MSB will be the parity bit (if set).
	 reg [1:0] state_reg;		//Statis register for state machine
	 reg [4:0] sample_count;	//Counts how many sample were taken
	 reg write_reg;				//Register for the write signal
	 
	 wire parity;			//Parity check line
	 wire decision;		//The result of vote.
	 
	 always @(posedge clk)
	 begin
		if(rst)									//When reset occures
			begin	
			state_reg <= IDLE;				//The begining state is IDLE
			data <= 10'b1111111111;			//Staring value of data is ones in every bit
			sample <= 0;						//Starting value of sample is zero
			sample_count <= 0;				//Caunter set to zero
			write_reg <= 0;					//Write reg is set to low
			end
		else
			begin
			case (state_reg)
				IDLE :										//The reciever is ready to reciev
					if(!in)									//When the input line goes to low it may be indicates a start bit
						begin
						state_reg <= STARTING;			//Switching to starting state
						data <= 10'b1111111111;			//Staring value of data is ones in every bit
						sample <= 0;						//Starting value of sample is zero
						sample_count <= 0;				//Caunter set to zero
						end
					else
						write_reg <= 0;					//Write reg is set to low
				STARTING :									//There was a zero on the input line it can be a start bit
					if(!decision && (sample_count == 5'd16))	//The input line in the minddle of the cicle was law
						begin
						state_reg <= RECIEVING;			//Switching to recieving stare
						sample_count <= 0;				//Resets the caunter
						data[9] <= 1'b0;					//Writes the start bit into the data input register
						end
					else if(rate)							//When a pusle comes
						begin
						sample_count <= sample_count + 1;	//Increments the sample caunter
						sample <= {in, sample[15:1]};			//Put the sample in the MSB and shifts the sample register rights
						end
				RECIEVING :									//There was a start bit. The reciever is now recieving
					if(!data[0] || (!PARITY && !data[1]))	//When the start bit shifted to the LSB os LSB + 1
						begin
						state_reg <= FINISHED;
						if(!PARITY)									//When there is no parity bit
							data <= {1'b0, data[9:1]};			//One more bit is needed for correct data from
						end
					else if(sample_count == 5'd16)			//If there was 16 count
						begin
							sample_count <= 0;					//Resets the counter
							data <= {decision, data[9:1]};	//Puts new data on the MSB and shifts the register one to the right
						end
					else if(rate)									//When a rate comes
						begin
						sample_count <= sample_count + 1;	//Increments the sample caunter
						sample <= {in, sample[15:1]};			//Put the sample in the MSB and shifts the sample register rights
						end
				FINISHED : 
					begin
					state_reg <= IDLE;
					write_reg <= 1'b1;
					end
				default : state_reg <= IDLE;
			endcase
		end
	end
	 
	 assign decision = ((sample[7] & sample[8]) || (sample[8] & sample[9]) || (sample[7] & sample[9]));
	 
	 //Party checkher module
	 parity_checker_8bit parity_module(
		.parity_bit(PARITY_BIT),				
		.data(data[8:1]),
		.parity_out(parity)
	 );
	 
	 assign write = write_reg;
	 assign err = PARITY & (data[9]^parity);
	 assign data_out = data[8:1];
	

endmodule
