`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    18:33:49 02/22/2017 
// Design Name: 
// Module Name:    parity_checker_8bit 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: Asyncronous 8bit parity checker with selectable parity setting.
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module parity_checker_8bit(
    input parity_bit,					//If 0 even parity, when 1 then odd parity
    input [7:0] data,
    output parity_out
    );
	 
	 //assign parity_bit = 0;
	 
	 
	 assign par = (((((((data[0]^data[1])^data[2])^data[3])^data[4])^data[5])^data[6])^data[7]);
	 
	 assign parity_out = parity_bit^par;
	 
	 


endmodule
