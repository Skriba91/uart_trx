`timescale 1ns / 1ps

////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer:
//
// Create Date:   17:10:18 02/27/2017
// Design Name:   uart_trx_cntr
// Module Name:   C:/Users/VSkriba/Documents/Xilinx/Projects/uart_trx/sim_uart_trx_cntr.v
// Project Name:  uart_trx
// Target Device:  
// Tool versions:  
// Description: 
//
// Verilog Test Fixture created by ISE for module: uart_trx_cntr
//
// Dependencies:
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
////////////////////////////////////////////////////////////////////////////////

module sim_uart_trx_cntr;

	// Inputs
	reg clk;
	reg btn_rst;
	reg write_tx_buffer;
	reg read_rx_buffer;
	reg [7:0] data_to_send;
	reg rx_data;

	// Outputs
	wire tx_data;
	wire [7:0] data_recieved;
	wire input_buffer_full;
	wire output_buffer_full;
	wire error;

	// Instantiate the Unit Under Test (UUT)
	uart_trx_cntr uut (
		.clk(clk), 
		.btn_rst(btn_rst), 
		.write_tx_buffer(write_tx_buffer), 
		.read_rx_buffer(read_rx_buffer), 
		.data_to_send(data_to_send), 
		.rx_data(rx_data), 
		.tx_data(tx_data), 
		.data_recieved(data_recieved), 
		.input_buffer_full(input_buffer_full), 
		.output_buffer_full(output_buffer_full), 
		.error(error)
	);

	initial begin
		// Initialize Inputs
		clk = 0;
		btn_rst = 0;
		write_tx_buffer = 0;
		read_rx_buffer = 0;
		data_to_send = 0;
		rx_data = 0;

		// Wait 100 ns for global reset to finish
		#50;
		btn_rst = 1;
		#15
		data_to_send = 8'd52;
		write_tx_buffer = 1;
		#10
		data_to_send = 8'd50;
		#30
      write_tx_buffer = 0;
		// Add stimulus here

	end
	
	always #1
		rx_data <= tx_data;
		
	always #10
		clk <= ~clk;
      
endmodule

