`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date:    20:07:35 02/25/2017 
// Design Name: 
// Module Name:    uart_trx_cntr 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module uart_trx_cntr(
    input clk,								//Clock
    input btn_rst,						//Reset button
	 input write_tx_buffer,				//Write signal for output buffer
	 input read_rx_buffer,				//Read signal for input buffer
	 input [7:0] data_to_send,			//Data to write in output buffer
	 input rx_data,						//Input data from serial port
    output tx_data,						//Out put data on serial port
	 output [7:0] data_recieved,		//Data in input FIFO
	 output input_buffer_full,			//Input data buffer full flag
	 output output_buffer_full,		//Output data buffer full flag
	 output error
    );
	 
	 localparam SIZE_TX = 4'd8, RATE_DIV_TX = 9'd434;		//Parameters for transmitter rate generator 115200bps
	 localparam SIZE_RX = 4'd4, RATE_DIV_RX = 5'd27;		//Parameter for reciever rate generator
	 localparam PARITY = 1'b1, PARITY_BIT = 1'b0;			//Parameters for parity bit
	 
	 reg rst;
	 always @(posedge clk)
	 begin
		rst <= !btn_rst;			//Reset button on FPGA panel is on pullup
	 end
	 
	 wire [7:0] out_data_buff_tx;		//wire from output buffer to transmitter
	 wire [7:0] in_data_rx_buff;		//wire from reciever to input buffer
	 
	 
	 //Data buffer for transmitter
	 fifo_128byte output_buffer (
    .clk(clk), 
    .rst(rst),
    .write(write_tx_buffer), 
    .read(read_tx), 
    .data_in(data_to_send), 	//Data to send
    .data_av(data_av_tx), 
    .full(output_buffer_full), 
    .data_out(out_data_buff_tx)		//Data for transmitter
    );
	 
	 //Data buffer for reciver
	 fifo_128byte input_buffer (
    .clk(clk), 
    .rst(rst),
    .write(write_rx), 
    .read(read_rx_buffer), 
    .data_in(in_data_rx_buff), 	//Data from reciver
    .data_av(data_av_rx), 
    .full(input_buffer_full), 
    .data_out(data_recieved)	//Recieved data
    );
	 
	 
	 rategen #(
	 .SIZE(SIZE_TX),
	 .RATE_DIV(RATE_DIV_TX))
	 tx_rategen (
    .clk(clk), 
    .rst(rst),
    .rate(tx_rate)
    );
	 
	 rategen #(
	 .SIZE(SIZE_RX),
	 .RATE_DIV(RATE_DIV_RX))
	 rx_rategen (
    .clk(clk), 
    .rst(rst),
    .rate(rx_rate)
    );

	data_cntr #(
	.PARITY(PARITY),
	.PARITY_BIT(PARITY_BIT))
	tx_data_cntr (
    .clk(clk), 
    .rst(rst), 
    .rate(tx_rate), 
    .data_av(data_av_tx), 
    .data_in(out_data_buff_tx), 
    .read(read_tx), 
    .finished(finished), 
    .data_out(tx_data)
    );
	 
	 rx_data_cntr #(
	 .PARITY(PARITY),
	 .PARITY_BIT(PARITY_BIT))
	 rx_data_cntr (
    .clk(clk), 
    .rst(rst), 
    .rate(rx_rate), 
    .in(rx_data), 
    .write(write_rx), 
    .err(error), 
    .data_out(in_data_rx_buff)
    );


endmodule
