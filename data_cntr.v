`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: Skriba D�niel
// 
// Create Date:    20:45:57 02/11/2017 
// Design Name: 
// Module Name:    data_cntr 
// Project Name: 
// Target Devices: 
// Tool versions: 
// Description: 
//
// Dependencies: 
//
// Revision: 
// Revision 0.01 - File Created
// Additional Comments: 
//
//////////////////////////////////////////////////////////////////////////////////
module data_cntr
	#(parameter PARITY = 1'b0, PARITY_BIT = 1'b0)
	(
    input clk,					//clock
    input rst,					//reset signal
    input rate,				//transmitter clock
    input data_av,			//true state of this flag indicates that there is data in output buffer ready to send
    input [7:0] data_in,	//paralel data input for tansmit
	 output read,				//flag that sign that data was read out from output buffer
	 output finished,			//data sending finished
    output data_out			//serial data output		
    );
	 
	 
	 //Module for parity check if parity check is enabled
	 parity_checker_8bit parity_module(
		.parity_bit(PARITY_BIT),				
		.data(data_in),
		.parity_out(parity)
	 );
	 
	 reg [10:0] data;
	 reg read_reg;
	 
	 //This block reads and format data and shift it out
	 always @(posedge clk)
	 begin
		if(rst) 					//reset state
		begin
			data <= 1'b1;			//reset sets output line high ready to send
			read_reg <= 0;
		end
		else if(data_av & finished & rate & PARITY) //If data is available and the last data was sent it reads the next data with parity
		begin
			data[0] <= 1'b0;				//sets the first bit to start bit
			data[8:1] <= data_in;	//reads the data from output buffer
			data[9] <= parity;		//sets the parity bit
			data[10] <= 1'b1;				//sets the stop bit
			read_reg <= 1'b1;				//sets the read flag to high indicating data was read
		end
		else if(data_av & finished & rate) //If data is available and the last data was sent it reads the next data without parity
		begin
			data[0] <= 1'b0;				//sets the first bit to start bit
			data[8:1] <= data_in;	//reads the data from output buffer
			data[9] <= 1'b1;				//sets the stop bit
			data[10] <= 1'b0;				//Not used bit
			read_reg <= 1'b1;				//sets the read flag to high indicating data was read
		end
		else if(rate & !finished)		//Until the last digit reaches the stop bit it shifts zeroes in data register
			data <= {1'b0, data[10:1]};
		else									//Defult case
			read_reg <= 1'b0;				//Sets read to zero, ready for another read
	 end
	 
	 //Puts the last bit of data register to output line
	 assign data_out = data[0];
	 //Writes read state to output line
	 assign read = read_reg;
	 
	 assign finished = (data == 11'd1);


endmodule
