/**********************************************************************/
/*   ____  ____                                                       */
/*  /   /\/   /                                                       */
/* /___/  \  /                                                        */
/* \   \   \/                                                       */
/*  \   \        Copyright (c) 2003-2009 Xilinx, Inc.                */
/*  /   /          All Right Reserved.                                 */
/* /---/   /\                                                         */
/* \   \  /  \                                                      */
/*  \___\/\___\                                                    */
/***********************************************************************/

#include "xsi.h"

struct XSI_INFO xsi_info;



int main(int argc, char **argv)
{
    xsi_init_design(argc, argv);
    xsi_register_info(&xsi_info);

    xsi_register_min_prec_unit(-12);
    work_m_00000000002840303492_1353337225_init();
    work_m_00000000001736589731_3588054307_init();
    work_m_00000000001736589731_2096836882_init();
    work_m_00000000003440059365_2698286115_init();
    work_m_00000000004130640690_0697239288_init();
    work_m_00000000003371426345_0689103655_init();
    work_m_00000000000268733356_0928666626_init();
    work_m_00000000002488683164_4231673960_init();
    work_m_00000000004134447467_2073120511_init();


    xsi_register_tops("work_m_00000000002488683164_4231673960");
    xsi_register_tops("work_m_00000000004134447467_2073120511");


    return xsi_run_simulation(argc, argv);

}
